---
title: Change management
description: Support Operations documentation page for Pagerduty change management
canonical_path: "/handbook/support/readiness/operations/docs/pagerduty/change_management"
---

TBD, see https://gitlab.com/gitlab-com/support/support-ops/other-software/pagerduty/-/issues/92 for more info
