---
title: "Measurement Clarity"
summary: Outdated workforce supervision tactics often trigger bias and presenteeism. Evaluating productivity differently requires unique methods of recording and managing results.
canonical_path: "/teamops/measurement-clarity/"
images:
    - /images/opengraph/all-remote.jpg
---

![GitLab TeamOps teamwork illustration](/teamops/images/teamops-illustration_teamwork_purple.png)

{{% alert color="primary" %}}
This page is about one of the four Guiding Principles of TeamOps: Measurement Clarity. To learn more about the other three Principles, return to the main TeamOps page for a [complete overview of TeamOps](https://about.gitlab.com/teamops/), or enroll in the free [TeamOps course](https://levelup.gitlab.com/learn/course/teamops).
{{% /alert %}}

_Outdated workforce supervision tactics often trigger bias and presenteeism. Evaluating productivity differently requires unique methods of recording and managing results._

Measurement is important in any organization. As Peter Drucker famously said, "If you can't measure it, you can't manage it." But in conventionally managed organizations, too often the model of "successful work" involves physical presence—a team member needs to be in a certain building in order to be "at work."

Teams working via TeamOps should employ methods of measuring productivity, value, and results that _don't_ depend on physical supervision as a measure of contribution. Re-prioritizing _what_, _how_, and _when_ the organization measures enables a higher frequency of success analysis, greater accountability for objectives, lower workforce discrimination, and wider reach for company communication.

In short: TeamOps treats "work" as a verb, not a noun.

Action tenets of measurement clarity, including real-world examples of each, are below.

## Iteration

Conventionally managed organizations often increase pressure to present complete and polished projects, documents, or plans when work "has finished." But this expectation slows progress and expends valuable time that could be used to conduct multiple rounds of feedback on smaller changes.

Executing on a decision shouldn't be a singular, one-time event. TeamOps reframes execution as an _ongoing series_ of [iterations](https://about.gitlab.com/values/#iteration)—small, compounding results each worthy of celebration. This encourages [smaller steps](https://about.gitlab.com/values/#move-fast-by-shipping-the-minimal-viable-change), which are more amenable to feedback, course correction, easy delivery. Breaking decisions into manageable components makes obtaining desired results more feasible.

A key aspect of TeamOps is incorporating [iteration](https://about.gitlab.com/values/#iteration) into every process and decision. This requires team members to act with a relatively [low level of shame](https://about.gitlab.com/values/#low-level-of-shame)—that is, to resist the urge to conceal work until it's perfect for fear of making mistakes. Working this way means doing the smallest viable and valuable thing (the [minimal viable change, or MVC](https://about.gitlab.com/values/#minimal-viable-change-mvc)) as quickly as possible in order to solicit feedback. Despite initial discomfort involved in this kind of sharing, iteration enables faster execution, shorter feedback loops, and an ability to course-correct sooner.

Ultimately, the goal for fast-moving teams is to reduce the time between making a decision and getting the result to market. Taking an iterative approach to everyday operations can help a project, team, or company in any industry achieve this goal.

{{% alert title="Tip" color="primary" %}}
To empower even more of your team to make fast decisions through iteration, consider hosting [Iteration Office Hours](https://www.youtube.com/watch?v=2eA7-E950ps).
{{% /alert %}}

{{% details summary="Example of iteration" %}}
**Example 1:** [Iterating on promotional videos to launch TeamOps](https://gitlab.com/gitlab-com/marketing/inbound-marketing/global-content/digital-production/-/issues/319)

During development of TeamOps, our team at GitLab aspired to produce [two high-quality videos](https://gitlab.com/gitlab-com/marketing/inbound-marketing/global-content/digital-production/-/issues/319) to introduce the first, internal iteration of TeamOps certification. When it became clear that TeamOps' brand identity would be changing in the coming weeks, we decided to produce only one video—an example of [Minimal Viable Change (MVC)](https://about.gitlab.com/values/#minimal-viable-change-mvc)—and iterate as new brand guidelines took shape. We celebrated this [boring solution](https://about.gitlab.com/values/#boring-solutions) (one video, the minimum required to inform GitLab team members), which enabled faster decisions throughout the launch phase.

**Example 2:** [Adding an MVC GitLab Citation Index to GitLab for Education homepage](https://gitlab.com/gitlab-com/www-gitlab-com/-/merge_requests/69665/)

A conventionally managed organization might treat a homepage revision in a binary fashion: it's either complete or it's not. [This merge request](https://gitlab.com/gitlab-com/www-gitlab-com/-/merge_requests/69665/) details a minimum viable change (MVC) to add one element to the [GitLab for Education](https://about.gitlab.com/solutions/education/) homepage. In the comment thread, GitLab team members agree that this iteration moves the page one step closer to an ideal state. By embracing and celebrating each iteration _as_ execution, team members accelerate execution on other projects rather than being stuck on a prior project.
{{% /details %}}

## Definition of done

While often associated with the [Agile](https://www.agilealliance.org/glossary/definition-of-done/) software development movement, "definition of done" is a practice that teams in _any_ industry can use to enhance measurement clarity. For organizations practicing TeamOps, developing a definition of done helps teams identify the conclusion of one [iteration](/teamops/measurement-clarity/#iteration) and the beginning of the next. For [software development teams](https://docs.gitlab.com/ee/development/contributing/merge_request_workflow.html?_gl=1*1s8xk24*_ga*MTU4MjYxNTAxOS4xNjc0NjYwMDUy*_ga_ENFH3X7M5Y*MTY3NzI3MTAzMy42Ny4xLjE2NzcyNzI3MDcuMC4wLjA.#definition-of-done), this usually means that the code is verified as working; for other teams, it may mean that a certain output has been created and delivered.

A team's definition of done may take different shapes. It might outline the success criteria for a quarterly OKR, explain when a project or working group should be considered "closed," or enumerate the fulfillment criteria for a small task. Regardless of the scope of impact, clearly [communicating](/teamops/shared-reality/#low-context-communication) and [documenting](/teamops/shared-reality/#single-source-of-truth-ssot) completion criteria as a definition of done helps TeamOps teams align more closely on methods for measuring and define success.

Even better, definitions of done are prompts for important [team-building communication](/teamops/shared-reality/#informal-communication). When all of the criteria are complete, that's a signal to your team to review the [contributions each team member has made to the work](/teamops/everyone-contributes/), collaboratively brainstorm [future iterations](/teamops/measurement-clarity/#iteration), and celebrate the fulfillment of your [shared reality goals](/teamops/shared-reality/).

{{% details summary="Example of definition of done" %}}
**Example:** TeamOps Course Updates

Every quarter, GitLab's Workplace team adds new features and enhancements to the [free TeamOps course in LevelUp](https://levelup.gitlab.com/learn/course/teamops). Each [project management plan](https://gitlab.com/gitlab-com/ceo-chief-of-staff-team/workplace/teamops-fy24-q1/-/issues/5) includes a Measurement Clarity section outlining a list of completion criteria. This creates a very clear distinction between work that's _In Progress_ and _Closed_, and generates a signal to close the issue.
{{% /details %}}

## Prioritize due dates over scope

TeamOps may not treat [elapsed time as a success measurement](/teamops/measurement-clarity/#measure-results-not-hours), but it does require due dates. Under TeamOps, however, due dates aren't a means of creating unnecessary rigidity or measuring the duration of contributions; they exist to force mechanisms that enable teams to execute on decisions and enforce accountability.

An organization practicing TeamOps will always [set a due date](https://about.gitlab.com/values/#set-a-due-date) and, if necessary in light of changing circumstances, will _cut project scope_ to meet that due date rather than _postpone_ that date. This encourages teams to think [iteratively](/teamops/measurement-clarity/#iteration), recalculate the scope of current work, and better determine which aspects of a project are best saved for future objectives. Working this way limits loss of momentum.

{{% details summary="Example of prioritize due dates over scope" %}}
**Example 1:** [Maintaining a monthly release cadence for 10+ years](/releases/)

As of 2022-12-01, GitLab has [shipped a monthly product release](/releases/) for 134 consecutive months. That's more than 10 years! A decade of [consistent execution](/blog/2018/11/21/why-gitlab-uses-a-monthly-release-cycle/) is made possible by [cutting scope](https://about.gitlab.com/values/#set-a-due-date) instead of pushing ship dates.
{{% /details %}}

## Transparent measurements

Conventional management philosophies glorify "metrics," which is a nonspecific term that often lacks connection to objectives, mission, values, workflows, strategy, or a shared reality. TeamOps prefers [Key Performance Indicators (KPIs)](https://about.gitlab.com/company/kpis/), which are smaller, incremental measures linked to [Objectives and Key Results](https://about.gitlab.com/company/okrs/) (OKRs) that, well, _indicate performance_. These offer greater context on both daily operations and the relevance of ongoing productivity to a function or the entire company.

While KPIs measure smaller units than OKRs do, the former aren't dependent on the latter. In fact, the two should be symbiotic in nature, informing and influencing each other to enhance operational visibility, measurement accuracy, and team empowerment. If you're not creating OKRs to improve KPIs, then you're either missing KPIs or you have the wrong OKRs.

Crucially, under TeamOps every functional department shares its KPIs [transparently shared](https://about.gitlab.com/values/#findability) across the organization. This aids visibility and enables everyone to contribute.

{{% details summary="Example of transparent measurements" %}}
**Example 1:** [Chief Executive Officer OKR and KPIs](https://about.gitlab.com//company/okrs/fy23-q3/)

In Q3-FY23 at GitLab, a CEO OKR was [Improve user and wider-community engagement](https://about.gitlab.com//company/okrs/fy23-q3/). This is the _initiative_ to improve a series of KPIs, a subset of which are documented below:

1. Evolve the resident contributor strategy by conducting 5 customer conversations with current “resident contributors” in seat
1. Certify 1,000 team members and 10,000 wider-community members in TeamOps
1. Enhance Corporate Processes and Successful Corporate Development Integration & Prospecting

These are documented in a tool that's accessible to the entire organization. Any team member can see any function's OKRs and KPIs for the quarter, reinforcing the [value of transparency](https://about.gitlab.com/values/#transparency).
{{% /details %}}

## Measure results, not hours

Every organizational operational model aims to optimize the efficiency with which teams produce results. But conventionally managed teams make a critical error when they conflate "efficiency" with "speed." Doing so means _time_ often becomes the team's highest priority—and _working hours_ become a principal success metric for the organization.

In organizations powered by TeamOps, team members understand that the root of "productivity" is "to produce," and therefore focus on [executing business results](https://about.gitlab.com/values/#measure-results-not-hours), rather than executing on [presenteeism](https://language.work/research/killing-time-at-work/). TeamOps therefore encourages success measurements based on outputs, not inputs.

Note that outputs aren't just tangible deliverables. Results include any form of value a team member contributes to the organization's shared reality: helping a teammate, satisfying a customer, shipping code, brainstorming a new idea, writing a revision, or researching a competitor. All quantifiable reports, messages, insights, or submissions are evidence of productivity.

{{% details summary="Example of measure results, not hours" %}}
**Example 1:** [Measuring impact of GitLab's 10 year campaign](https://gitlab.com/gitlab-com/marketing/corporate_marketing/corporate-marketing/-/issues/5507)

Producing the `10 Years of GitLab` integrated marketing campaign and [associated website](https://about.gitlab.com/ten/) demanded a cross-functional effort. Working group members established a GitLab issue to explicitly define [elements to be tracked and measured](https://gitlab.com/gitlab-com/marketing/corporate_marketing/corporate-marketing/-/issues/5507) in order to provide an adequate report of the campaign's overall impact. By focusing on results over hours spent (or if a given team member was online at a certain time, or in a certain office), everyone involved in the project could focus energy on executing the campaign.
{{% /details %}}


## Transparent feedback

Achieving measurement clarity involves more than evaluating _whether_ your team was successfully achieved its goals. It also involves evaluating _how_ your team was successful during the process. So just as a team's measurements should be as [transparent](/teamops/measurement-clarity/#transparent-measurements) as possible, so too should be a team's feedback _about_ those results.

[Transparent feedback](https://about.gitlab.com/handbook/people-group/guidance-on-feedback/) means that constructive criticism is _immediate_, _specific_, and _documented_. Like all information shared on a team practicing TeamOps, the impacts and action items a message triggers shouldn't rely on a recipient's subjective memory or interpretation. Sharing feedback is a great start to improving an individual's future work; sharing it _transparently_ means that the entire team can improve _together_.

To optimize the efficacy of delivered feedback, consider how other TeamOps tenets can support actionability. For example:

- Exercise your [bias for action](/teamops/decision-velocity/#bias-for-action) to share feedback as quickly as possible
- Use [low-context communication](/teamops/shared-reality/#low-context-communication) by sharing specific examples that support your feedback
- Leverage [asynchronous workflows](/teamops/everyone-contributes/#asynchronous-workflows) to allow time for review and reflection
- Encourage [iterative](/teamops/measurement-clarity/#iteration) growth to build up to aspirational goals
- In appropriate scenarios, share feedback (both positive and negative) [publicly](/teamops/shared-reality/#public-by-default) with an entire group whose other members can also learn from the insights.
- As a manager, prioritize building psychological safety by reinforcing a [short toes](/teamops/everyone-contributes/#short-toes) mindset during group collaboration, encouraging healthy controversy by having [strong opinions weakly held](/teamops/decision-velocity/#strong-opinions-weakly-held), and by investing time in [informal communication](/teamops/shared-reality/#informal-communication) to build trust and camaraderie that can withstand negative criticism.

{{% details summary="Example of transparent feedback" %}}
**Example:** [A member of GitLab's L&D Team Giving Feedback to the CEO](/handbook/people-group/guidance-on-feedback/#guidance-on-giving-and-receiving-feedback)

At GitLab, our mission that [everyone can contribute] even influences our feedback guidelines—suggesting that any team member, at any level, can give feedback to any other team member, at any level. In this video about [Guidance on Giving and Receiving Feedback](/handbook/people-group/guidance-on-feedback/#guidance-on-giving-and-receiving-feedback), the CEO of GitLab, Sid Sijbrandij, discusses this challenge in more detail, and asks for performance feedback from a member of the Learning & Development team.
{{% /details %}}

## Cadence

In organizations built on information-based operations, team members' collective sense of stability, security, and well-being is an outgrowth of their knowing when future opportunities to receive and exchange knowledge will occur. A [Single Source of Truth (SSoT)](/teamops/shared-reality/#single-source-of-truth-ssot) and [asynchronous workflows](/teamops/everyone-contributes/#asynchronous-workflows) ensure that existing information is continuously accessible. But what about informational _updates_? Not knowing about emerging decisions, forthcoming goals, or adjustments to long-term visions can compromise a team's focus, efficiency, and trust.

This is why establishing a transparent [cadence](https://about.gitlab.com/company/cadence/) for decision-making activities, informational updates, and feedback opportunities is important for teams practicing TeamOps. A regular cadence sets a _pace_ for productivity and creates predictable, comfortable intervals for work. Establishing and documenting a cadence for everything from operational workflows and [due dates](https://about.gitlab.com/handbook/teamops/measurement-clarity/#prioritize-due-dates-over-scope)  to company announcements and team meetings can prevent the kinds of distraction and burnout that often result from context switching, distractive research, or individual uncertainty.

{{% details summary="Example of cadence" %}}
Example: [GitLab’s Quarterly All-Hands Meeting](/company/gitlab-assembly/)

At the same time each quarter, executive leadership hosts [GitLab Assembly](/company/gitlab-assembly/)—a company-wide recap of the past quarter's accomplishments, summary of the new quarter's objectives, and an open-floor Q&A for any employee to resolve questions or concerns. Knowing exactly when this meeting will occur, who will be in attendance, and what will be discussed gives GitLab team members full confidence of when they can have direct access to the executive team about company growth.
{{% /details %}}

---

{{< cardpane >}}

{{< panel header="**Home**" header-bg="indigo" >}}
Return to the <a href="https://about.gitlab.com/teamops/">TeamOps homepage</a>.
{{< /panel >}}

{{< panel header="**Next**" header-bg="success" >}}
Restart the TeamOps model by learning about [TeamOps - Shared Reality](/teamops/shared-reality/)
{{< /panel >}}

{{< /cardpane >}}

