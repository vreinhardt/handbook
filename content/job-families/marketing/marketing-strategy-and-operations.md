---
title: "Marketing Strategy and Platforms"
---

## Levels

### Sr. Director, Marketing Strategy and Platforms

#### Sr. Director, Marketing Strategy and Platforms Job Grade

The Director, Marketing Strategy is a [grade 11](https://about.gitlab.com/handbook/total-rewards/compensation/compensation-calculator/#gitlab-job-grades).

#### Sr. Director, Marketing Strategy and Platforms Responsibilities

- Build, lead, and inspire a team of highly skilled analytics professionals, web developers, and marketing operations professionals who deliver impactful business insights and results
- Manage the global marketing planning process including headcount and program spend.
- Partner with Marketing Leadership to formulate and develop regional/segment marketing plans and constantly evolving go-to-market strategies
- Develop and manage executive reporting on key metrics, formulate actionable insights and structure a concise, clear presentation of findings and prioritize issues as appropriate
- Create Board level and other internal presentations for the Chief Marketing Officer
- Collaborate with Marketing Leaders and teams to develop and execute marketing management disciplines and processes (weekly/quarterly forecast, pipeline analysis and development, target account lists, marketing OKRs, marketing overall campaign framework, budget allocation).
- Align go-to-market motions with sales. Identify opportunities to improve go-to-market efficiencies and lead efforts to scale, align and invest in the business in partnership with sales strategy and finance
- Lead, manage and evaluate the marketing tools, processes, policies and programs to ensure continuous productivity and effectiveness of marketing team members and marketing programs.
- Articulate a vision for the end-to-end digital website and experimentation framework and execute marketing programs against that vision
- Enable rapid A/B testing framework for use across the end-to-end customer journey
- Partner with Product Growth, Sales Systems, Business technology and Data team for a data collection strategy for the end-to-end customer journey
- Collaborate with marketing, and sales stakeholders, product managers, and other GitLab teams to develop marketing operations roadmap and provide input into prioritization
- Drive cross functional marketing programs such as product launch and introduction and localization. Drive key initiatives and projects for the Chief Marketing Officer.
- Be a trusted advisor to Marketing Leadership

#### Sr. Director, Marketing Strategy and Platforms Requirements

- 15+ years of experience in a global SaaS marketing or marketing operations environment. Preference for Marketing Strategy, Marketing Operations, Strategy Consulting or Corporate Strategy
- BA/BS degree, MBA Preferred
- Excellent quantitative analytical skills, creativity in problem solving, and a keen business sense.
- Experience with marketing attribution models.
- Experience in analyzing, experimenting and driving results in marketing websites.
- Ability to think strategically, but also have exceptional attention to detail to drive program management and execution
- Familiarity with our [tech stack](https://about.gitlab.com/handbook/marketing/marketing-operations/#-tech-stack) is a strong plus
- Experience with SQL, Tableau, and/or similar analytical packages a plus
- Ability to conduct sophisticated and creative analysis, yet translate those results to easily digestible messages, communications, and presentations
- Interest in GitLab, and open source software
- You share our values, and work in accordance with those values.
- Ability to thrive in a fully remote organization
- Ability to use GitLab

### VP, Marketing Strategy and Platforms

#### VP Marketing Strategy and Platforms Job Grade

The VP, Marketing Strategy is a [grade 12](https://about.gitlab.com/handbook/total-rewards/compensation/compensation-calculator/#gitlab-job-grades)).

#### VP, Marketing Strategy and Platforms Responsibilities

- Extends that of the Sr. Director, Marketing Strategy and Platforms responsibilities
- Thought leader defining overall go-to-market data collection
- Thought leader defining GitLab’s approach to A/B testing
- Lead creation of multiple key marketing strategy initiatives and programs with CMO.

#### VP, Marketing Strategy and Platforms Requirements

- Extends that of the  Sr. Director, Marketing Strategy and Platforms
- 20+ years of experience in a global SaaS marketing or marketing operations environment. Preference for Marketing Strategy, Marketing Operations, Strategy Consulting or Corporate Strategy
Requirements

## Performance Indicators

- [Marketing efficiency ratio](https://about.gitlab.com/handbook/marketing/performance-indicators/#marketing-efficiency-ratio)
- INQ to SAO conversion rate
- [Total number of SAOs by month](https://about.gitlab.com/handbook/marketing/performance-indicators/#total-number-of-mqls-by-month)

## Career Ladder

The next step in the Marketing Strategy job family beyond director is not yet defined at GitLab.

## Hiring Process

Candidates for this position can expect the hiring process to follow the order below. Please keep in mind that candidates can be declined from the position at any stage of the process. To learn more about someone who may be conducting the interview, find their job title on our [team page](https://about.gitlab.com/company/team/).

- Qualified candidates will be invited to schedule a 30 minute screening call with one of our Global Recruiters.
- Next, candidates will be invited to speak with the Chief Marketing Officer.
- Next, candidates will be invited to speak with the Sr. Director of Sales strategy
- Next candidates will meet two of CMO staff: AVP of Sales Development, VP of Corporate Marketing, VP of Portfolio Marketing, or VP Demand Generation
- Next, candidates will be invited to meet with a panel of their future direct reports Director of Marketing Strategy, Director of Marketing Operations and Head of Digital Experience
- Finally, candidates will meet either Chief People Officer or Chief Revenue Officer

Additional details about our process can be found on our [hiring page](https://about.gitlab.com/handbook/hiring/).
