---
layout: handbook-page-toc
title: "TeamOps Growth"
canonical_path: "/handbook/teamops/growth/"
images:
    - /images/opengraph/all-remote.jpg
---

The future of work is about human empowerment. So, as a company, we at GitLab support both strategies of work-life balance and employee experience through our human-centric operational model: TeamOps. Based on autonomous ways of working. TeamOps enables users (internally or externally) to enjoy the schedule flexibility that comes along with async-first, handbook-first operations, but also activates an attractive work environment in workflows and processes, including:

- **Psychological Safety** – In this era of stronger diversity and inclusion, we all know the importance of inclusivity and belonging. However, TeamOps takes this even further by enforcing protocols that prevent our tenacious proactivity from becoming offensive, and other protocols that encourage healthy debate and feedback from all levels of our workforce, so everybody has an opportunity to voice their opinion. Even further, our prioritization of iteration enables our team to embrace imperfection and never consider any result irreversible.
- **Trust and Empowerment** – To fuel intrinsic motivation, each team member is equipped to work with a bias for action and have as much ownership in their day-to-day work as possible. We "push decisions to the lowest level" and cultivate a strong self-management mindset to confirm that everyone feels that they have authority and control.
- **Performance Transparency** - Measurement clarity tools like OKRs, KPIs, and cadences are fairly traditional. But in TeamOps, GitLab has uniquely blended them with our mission that “everyone can contribute” to create progressive productivity management strategies that enables all team members to know exactly which days they need to dig deep, and which days are best for them to enjoy some of their unlimited PTO. We even go so far as to encourage our staff to reduce the scope of their work when they’re feeling overwhelmed, so they can still achieving all of their goals on time without risking job security.

These points are a reflection of GitLab’s evolution from prioritizing our advocacy of [All-Remote](https://about.gitlab.com/company/culture/all-remote/guide/) to prioritizing our evangelism of TeamOps. **We want the world to understand that our success as a fully-distributed organization is not because of _where_ we’re working, it’s because of _how_ we’re working.**

## TeamOps Learning Path

If you’re eager to learn more about TeamOps and fuel the success of it’s adoption in your team, there are several ways to become more involved:

![GitLab TeamOps learning path](/handbook/teamops/images/teamops_learning_path.png)

### TeamOps Learner

The best place for any TeamOps enthusiast to start in their evangelism journey is the [free TeamOps course](https://levelup.gitlab.com/courses/teamops) on GitLab’s learning platform, [LevelUp](https://levelup.gitlab.com/). This is an interactive way for learners to explore the principles and tenets of TeamOps, in addition to reading about them in the handbook.

### TeamOps Practitioner

Ready to transform inspiration into implementation? Guided by change management tools and workshops, TeamOps practitioners are able to identify their unique strengths and weaknesses in the model, and optimize for success. Click here to learn more about our adoption [support services](#teamops-consulting-services).

### TeamOps Trainer

Great change management processes require the support of empowered change agents. To help support the adoption and maintenance of this operating model, the TeamOps Trainer program upskills Practitioners on the measurement, management, and mentorship strategies critical to success and sustainability.

Internally, this program is hosted once per quarter and is open to any team members. When applications for the next session are opened, it is announced on both the #whats-happening-at-gitlab and #teamops Slack channels.

Externally, this workshop is available by request for current TeamOps practitioners. Please [contact](#teamops-consulting-services) the Workplace team to inquire about when the next cohort will be accepting applications.

### TeamOps Facilitator

Available only to internal team members, the TeamOps Facilitator certification program is an opportunity for passionate Practitioners and Trainers to upskill as topic thought leaders, and leverage their expertise through career development opportunities such as:

- Local speaking engagements
- Facilitation of internal and external TeamOps workshops
- Contribution to emerging TeamOps content and decision making

Participation as a TeamOps Facilitator is voluntary, unpaid, and is in addition to a team member’s primary role. To ensure that facilitation responsibilities do not conflict with any OKRs, manager approval is required as part of the application process.

Applications to become a TeamOps Facilitator open at the end of each TeamOps Trainer program. For instructions on how to apply, refer to the “Next Steps” section of the TeamOps Trainer program outline, or contact a member of the Workplace team on the #teamops Slack channel.

## TeamOps Consulting Services

While incubated within GitLab, TeamOps has been adapted and standardized to help any organization in any industry more effectively work in more virtual-first, handbook-first, and asynchronous-first ways. Our TeamOps department offers a variety of consulting and corporate training services to support your success.

| TeamOps Service | Price |
| --- | --- |
| One-hour "ask me anything" Q&A calls with an in-house TeamOps expert | $500 USD |
| Keynote presentations about the role of TeamOps in the future of work<br>(virtual or on-site, travel and lodging not included) | $3,000 USD |
| Live, facilitated TeamOps corporate training workshops<br>(maximum of 15 participants) | $5,000 USD |
| TeamOps train-the-trainer course and/or workshop for change agents, company leadership, and managers<br>(virtual, &lt;15 participants) | $7,500 USD |

To learn more about these services, please [contact the TeamOps team](https://forms.gle/YcWF2ndji7d6coKw5).
